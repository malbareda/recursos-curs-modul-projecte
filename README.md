# Recursos Curs Mòdul Projecte

Aquí penjaré els materials del Curs del Mòdul de Projecte
https://odissea.xtec.cat/course/view.php?id=115299



2. Un cop obert el diari d'aprenentatge, escriu una primera entrada amb la teva reflexió sobre el curs que acabem de començar.


Dia 0 i ja estem perdent el temps amb xorrades. Anem malament.



No penso fer servir imatges.


Mapa mental de PMBOK
https://www.mindomo.com/mindmap/pmbok-b0a527f74f4e4851973c15a8aceac596

Valoració: Em recorda a un Scrum classic? No se. Com a mínim tenen el bon criteri de demanar-te un PERT.


Presentació Acta
https://www.canva.com/design/DAFEiLcM4P8/WuRkZFxBgx502D2l4hQ6IA/view?utm_content=DAFEiLcM4P8&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton

Valoració: Afegir enllaços a Google Docs interns


EDT:
He fet dos genially similars, un per els conceptes generals i l'altre amb els passos, per donarho en dues sessions diferents. El primer linka al segon

https://view.genial.ly/62b5fe4dfe601e00116dbb34/guide-lista-videoconsola

https://view.genial.ly/62b61258fe601e00116dd3f9/guide-lista-videoconsola


Valoració:  Això aniria com a part de la proposta prèvia de projecte


Riscos:


La gestió de riscos esta pensada en un ambient previ al Mòdul de Projecte a on es fa la proposta de projecte
https://view.genial.ly/62b62a54c9af90001879b572/interactive-content-mapa-mental-panal

Valoració: La matriu de riscos podria incorporar-se a la proposta prèvia. La resta és massa farregos.


Costos:


La gestió de costos esta pensada en un ambient previ al Mòdul de Projecte a on es fa la proposta de costos
El podcast està en mp3 dintre d'aquest mateix gitlab

Valoració: No es parla d'un pla de finançament tot i que és important...


Projecte:

La meva idea es fer una cosa que sigui aplicable JA en un Institut per a un Mòdul de Projecte de DAM

Acta Constitució: El que he fet en comptes d'una acta d'una sola persona és una guia de com fer el GDD, amb exemples, que és el que equival a l'acta de constitució

https://docs.google.com/document/d/1AI0pioDagn6piG_BDbLyZTa5OGtx65rQG2hlMG0dfvs/edit

EDT: Quatre documents, la proposta de projecte amb la temporització del projecte

https://docs.google.com/document/d/1QVXNt2GOvR_lKAAhafrxgG24vMBMu37GVeI1M5CCOlY/edit

i aquí un exemple. Està enfocat cap a un sistema de Checkpoints fent servir la metodologia de SCRUM, tal i com vaig fer al mòdul 3.

https://docs.google.com/spreadsheets/d/1VJgKGPe8q2bIWQ1zdLwkkEUnxFN73zwDgmLyq4LHpEM/edit?usp=sharing

I un document intern de seguiment:

https://docs.google.com/spreadsheets/d/1rd-X76iFxszAvG41kdESRYi7gcxfbtXCrAe1rhm4RxM/edit?usp=sharing

I una rúbrica d'avaluació

https://docs.google.com/spreadsheets/d/1156jo97R7WpnDs38ceP12rTNE2Wm8C3GtR9uAbaQh_Q/edit?usp=sharing

Riscos: Model d'una matriu de riscos, amb exemples incorporats

https://docs.google.com/spreadsheets/d/1XziU5IqQRq8yOqYP5Tx50Xy9atlRC5TPjneOVHpuWyk/edit#gid=0

Costos: Dos documents

Un indicant com fer el Pla financier

https://docs.google.com/document/d/1bJglAwn87MUbDda60GNWB7s8DwKohdhrwlTOH_6iz9k/edit  (pàgina 4)

i un exemple

https://docs.google.com/document/d/1kqs8ORgeUaP7wdIlbUSvug2wjJPuvvPQovbc9EBleOg/edit?usp=sharing




